package com.silvioapps.recyclerviewsample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class ItemFragment extends Fragment implements IRecyclerViewOnClickListener {
    private View view = null;
    private Activity activity = null;
    private double progress = 0;
    private boolean out = false;
    private Thread thread = null;
    private int visibility = View.VISIBLE;
    private static ItemFragment instance = null;
    private Handler handler = null;
    private MyAsyncTask myAsyncTask = null;
    private TextView titleTextView = null;
    private TextView textTextView = null;
    private TextView addressTextView = null;
    private CustomActionBarControls customActionBarControls = null;
    private FloatingActionButton logoFloatingActionButton = null;
    private ImageView pictureImageView = null;
    private MapView mapView = null;
    private RecyclerView recyclerView = null;
    private ComentariosAdapter comentariosAdapter = null;
    private LinearLayoutManager linearLayoutManager = null;
    private List<Item.Comentarios> comentariosList = null;
    private Button callButton = null;
    private Button servicesButton = null;
    private Button addressesButton = null;
    private Button commentsButton = null;
    private Button favouritesButton = null;
    private ServicosFragment servicosFragment = null;
    private Item mItem = null;
    private CustomDialogFragment customDialogFragment = null;
    private ScrollView scrollView = null;

    public static ItemFragment getInstance(){
        if(instance == null){
            instance = new ItemFragment();
        }

        return instance;
    }

    public static void destroyInstance(){
        if(instance != null){
            instance = null;
        }
    }

    public void setActivity(Activity activity){
        this.activity = activity;
    }

    public void preload(String id){
        handler = new Handler();
        progress = 0;

        myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute(id);
    }

    public double getProgress(){
        return progress;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setRetainInstance(true);

        Utils.setWindowBackgroundColor(activity, R.color.colorBackgroundList);

        comentariosList = new ArrayList<>();
        comentariosList.clear();

        createCustomDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        this.view = layoutInflater.inflate(R.layout.fragment_item, viewGroup, false);

        if(this.view != null) {
            this.view.setVisibility(visibility);

            pictureImageView = (ImageView) this.view.findViewById(R.id.pictureImageView);

            titleTextView = (TextView) this.view.findViewById(R.id.titleTextView);

            textTextView = (TextView) this.view.findViewById(R.id.textTextView);

            logoFloatingActionButton = (FloatingActionButton) this.view.findViewById(R.id.logoFloatingActionButton);

            addressTextView = (TextView) this.view.findViewById(R.id.addressTextView);
            addressTextView.setPadding(0, 0, addressTextView.getPaddingRight() + Constants.ADDRESS_TEXT_VIEW_PADDING_RIGHT, 0);

            mapView = (MapView) this.view.findViewById(R.id.mapView);

            if (activity != null) {
                recyclerView = (RecyclerView) this.view.findViewById(R.id.recyclerView);
                linearLayoutManager = new LinearLayoutManager(activity);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setHasFixedSize(true);
                comentariosAdapter = new ComentariosAdapter(activity, comentariosList, this);
                recyclerView.setAdapter(comentariosAdapter);
            }

            callButton = (Button) this.view.findViewById(R.id.callButton);

            servicesButton = (Button) this.view.findViewById(R.id.servicesButton);

            addressesButton = (Button) this.view.findViewById(R.id.addressesButton);

            commentsButton = (Button) this.view.findViewById(R.id.commentsButton);

            favouritesButton = (Button) this.view.findViewById(R.id.favouritesButton);

            scrollView = (ScrollView) this.view.findViewById(R.id.scrollView);

            setValues(mItem);
        }

        return this.view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        if(getActivity() != null) {
            Utils.showActionBar((AppCompatActivity)getActivity());

            customActionBarControls = new CustomActionBarControls();
            customActionBarControls.setControls((AppCompatActivity) getActivity(), this);
            customActionBarControls.getTitleTextView().setText("");
            customActionBarControls.getIconImageView().setVisibility(View.VISIBLE);

            if(mItem != null) {
                updateActionBar(mItem);
            }
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            if (thread != null) {
                thread.interrupt();
            }

            out = true;

            if(myAsyncTask != null){
                myAsyncTask.cancel(true);
            }

            destroyCustomDialogFragment();
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        //TODO
    }

    protected void createCustomDialogFragment(){
        customDialogFragment = new CustomDialogFragment();
        customDialogFragment.setShowProgressBar(false);
        customDialogFragment.setShowIndeterminate(false);
        customDialogFragment.setShowTitle(false);
        customDialogFragment.setShowText(true);
        customDialogFragment.setTextSize(Constants.DIALOG_TEXT_SIZE);
        customDialogFragment.setLayout(R.layout.custom_dialog_fragment_layout);
        customDialogFragment.create(getFragmentManager(),CustomDialogFragment.FRAGMENT_TAG);
    }

    protected void showCustomDialogFragment(String text){
        if(customDialogFragment != null){
            customDialogFragment.setText(text);
            customDialogFragment.show();
        }
    }

    protected void dismissCustomDialogFragment(){
        if(customDialogFragment != null){
            customDialogFragment.dismissDialog();
        }
    }

    protected void destroyCustomDialogFragment(){
        if(customDialogFragment != null){
            customDialogFragment.destroyDialog();
        }
    }

    protected void call(String phone){
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(Constants.SCHEME_ACTION_DIAL, phone, null));
        if(intent != null) {
            startActivity(intent);
        }
    }

    protected void showPicture(String url){
        if(pictureImageView != null  && activity != null) {
            Point screenSize = Utils.getScreenSize(activity);

            if (screenSize != null) {
                int width = screenSize.x;
                int height = (screenSize.y / (100 / Constants.PICTURE_HEIGHT_PERCENT));//height of 35% of the screen

                Picasso.with(activity)
                        .load(url)
                        .resize(width, height)
                        .centerCrop()
                        .placeholder(R.drawable.no_image_yet)
                        .error(R.drawable.error_no_image)
                        .into(pictureImageView);
            }
        }
    }

    protected void showLogo(String url){
        if(logoFloatingActionButton != null && activity != null) {
            int fabSizeNormal = (int)getResources().getDimension(R.dimen.design_fab_size_normal);

            Picasso.with(activity)
                    .load(url)
                    .resize(fabSizeNormal, fabSizeNormal)
                    .centerInside()
                    .into(logoFloatingActionButton);
        }
    }

    protected void showMap(Bundle bundle, double latitude, double longitude, String markerTitle, String markerDescription, float cameraZoom){
        if(mapView != null && activity != null) {
            Point screenSize = Utils.getScreenSize(activity);
            if (screenSize != null) {
                int width = ViewGroup.LayoutParams.MATCH_PARENT;
                int height = (screenSize.y / (100 / Constants.MAP_HEIGHT_PERCENT));//height of 15% of the screen

                LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(width, height);

                if (mapView.getParent() instanceof LinearLayout) {
                    mapView.setLayoutParams(linearLayoutParams);
                }
            }

            final MyMapView myMapView = new MyMapView();
            myMapView.setSettings(latitude, longitude, markerTitle, markerDescription, cameraZoom, false);
            myMapView.startMapView(bundle, activity.getApplicationContext(), mapView);
        }
    }

    public void setVisibility(int visibility){
        this.visibility = visibility;

        if(this.view != null){
            this.view.setVisibility(visibility);
        }
    }

    protected void setValues(final Item item){
        if(item != null) {
            this.mItem = item;

            showPicture(item.getUrlFoto());

            showMap(null, item.getLatitude(), item.getLongitude(), "", item.getEndereco(), Constants.CAMERA_ZOOM);

            showLogo(item.getUrlLogo());

            if(titleTextView != null){
                titleTextView.setText(item.getTitulo());
            }

            if(textTextView != null){
                textTextView.setText(item.getTexto());
            }

            updateActionBar(item);

            if(addressTextView != null){
                addressTextView.setText(item.getEndereco());
            }

            if(comentariosList != null && comentariosList.isEmpty()) {
                comentariosList.addAll(item.getComentarios());
            }

            if (comentariosAdapter != null) {
                comentariosAdapter.notifyDataSetChanged();
            }

            if(callButton != null) {
                callButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        call(item.getTelefone());
                    }
                });
            }

            if(servicesButton != null){
                servicesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        createServicosFragment(getActivity());
                        loadServicosFragment();
                        showServicosFragment();
                    }
                });
            }

            if(addressesButton != null){
                addressesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showCustomDialogFragment(item.getEndereco());
                    }
                });
            }

            if(commentsButton != null){
                commentsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        scrollToComments();
                    }
                });
            }

            progress = Constants.FINISH_PROGRESS;
        }
    }

    protected void scrollToComments(){
        if(scrollView != null && recyclerView != null){
            Utils.scrollToAView(scrollView, recyclerView);
        }
    }

    protected void updateActionBar(Item item){
        if(customActionBarControls != null && customActionBarControls.getTitleTextView() != null) {
            customActionBarControls.getTitleTextView().setText(item.getCidade() + getString(R.string.separator) + item.getBairro());
        }
    }

    protected void createServicosFragment(Activity activity){
        servicosFragment = new ServicosFragment();
        servicosFragment.setActivity(activity);
        servicosFragment.setVisibility(View.GONE);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, servicosFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    protected void loadServicosFragment(){
        if(servicosFragment != null) {
            servicosFragment.preload();
        }
    }

    protected void showServicosFragment(){
        if(servicosFragment != null){
            servicosFragment.setVisibility(View.VISIBLE);
        }
    }

    private class MyAsyncTask extends AsyncTask<String, Object, Item> {
        private Item item = null;
        private boolean completed = false;
        private Retrofit retrofit = null;

        protected void onPreExecute() {
            completed = false;

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        protected Item doInBackground(String... ids) {
            if(ids != null && ids.length > 0 && retrofit != null) {
                searchAsynchronously(retrofit, ids[0]);
            }

            return item;
        }

        protected void onPostExecute(Item result) {
            setValues(result);
        }

        protected void searchAsynchronously(Retrofit retrofit, String id) {
            try {
                Item.ItemAPI serviceItem = retrofit.create(Item.ItemAPI.class);
                Call<Item> callItem = serviceItem.getObject(id);
                callItem.enqueue(new Callback<Item>() {
                    @Override
                    public void onResponse(Call<Item> call, Response<Item> response) {
                        item = response.body();

                        completed = true;
                    }

                    @Override
                    public void onFailure(Call<Item> call, Throwable t) {
                        Log.e("TAG", "onFailure " + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            while(!out && !completed){
                //TODO NOTHING
            }
        }
    }
}
