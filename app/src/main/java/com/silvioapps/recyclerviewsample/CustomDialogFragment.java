package com.silvioapps.recyclerviewsample;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by Silvio Guedes on 15/08/2016.
 */
public class CustomDialogFragment extends DialogFragment {
    public static final String FRAGMENT_TAG = "CustomDialogFragment";
    private static CustomDialogFragment instance = null;
    private ProgressBar progressBar = null;
    private Dialog dialog = null;
    private View view = null;
    private TextView textView = null;
    private boolean dismissed = true;
    private boolean isRunning = false;
    private boolean showTitle = false;
    private boolean showProgressBar = false;
    private boolean showTextView = true;
    private boolean showIndeterminate = false;
    private int layoutRes = -1;
    private int visibility = View.VISIBLE;
    private int progress = 0;
    private String text = "";
    private float textSize = 12.0f;

    public static CustomDialogFragment getInstance(){
        if(instance == null){
            instance = new CustomDialogFragment();
        }

        return instance;
    }

    public static void destroyInstance(){
        if(instance != null){
            instance = null;
        }
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(dialog == null) {
            dialog = new Dialog(getActivity());

            if (!showTitle && dialog != null) {
                if (!dialog.getWindow().hasFeature(Window.FEATURE_NO_TITLE)) {
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                }
            }

            if (dialog != null && layoutRes != -1) {
                dialog.setContentView(layoutRes);
            }
        }

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        this.view = layoutInflater.inflate(layoutRes, viewGroup, false);

        if(this.view != null) {
            progressBar = (ProgressBar)this.view.findViewById(R.id.indeterminateProgressBar);
            if(progressBar != null){
                if(showIndeterminate){
                    if(dialog != null) {
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    }

                    progressBar.setVisibility(View.VISIBLE);

                    hideAllDivisors();
                }
                else{
                    progressBar.setVisibility(View.GONE);

                    showAllDivisors();
                }
            }

            progressBar = (ProgressBar) this.view.findViewById(R.id.progressBar);
            if(progressBar != null) {
                if(showProgressBar){
                    progressBar.setVisibility(View.VISIBLE);
                }
                else{
                    hideProgressBarLayout();
                }

                progressBar.setProgress(progress);
            }

            textView = (TextView) this.view.findViewById(R.id.textView);
            if(textView != null){
                if(showTextView){
                    textView.setVisibility(View.VISIBLE);
                }
                else{
                     textView.setVisibility(View.GONE);
                }

                textView.setText(text);
                textView.setTextSize(textSize);
            }
        }

        return this.view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();

        isRunning = true;
    }

    @Override
    public void onResume(){
        super.onResume();

        isRunning = true;

        if(dismissed) {
            setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause(){
        super.onPause();

        isRunning = false;
    }

    @Override
    public void onStop(){
        super.onStop();

        isRunning = false;
    }

    @Override
    public void onDestroyView(){
        Dialog dialog = getDialog();
        // handles https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }

        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void onDismiss(DialogInterface dialog){
        dismissed = true;
    }

    public void create(FragmentManager fragmentManager, String tag){
        if(!this.isAdded()) {
            dialog = null;
            view = null;
            progressBar = null;
            dismissed = true;

            setVisibility(View.GONE);

            super.show(fragmentManager, tag);
        }
        else{
            setVisibility(View.VISIBLE);
        }
    }

    public void show(){
        dismissed = false;
        setVisibility(View.VISIBLE);
    }

    public void dismissDialog(){
        dismissed = true;
        setVisibility(View.GONE);
    }

    public void destroyDialog(){
        if(dialog != null) {
            dialog.dismiss();
        }

        dialog = null;
        view = null;
        progressBar = null;
        dismissed = true;
    }

    public void setVisibility(final int visibility){
        this.visibility = visibility;

        if(dialog != null){
            if(isRunning) {
                dialog.getWindow().getDecorView().setVisibility(visibility);

                if(visibility == View.VISIBLE) {
                    onStart();
                }
            }
        }
    }

    public void setShowProgressBar(boolean show){
        if(show){
            if(progressBar != null) {
                progressBar.setVisibility(View.VISIBLE);
            }

            showProgressBar = true;
        }
        else{
            if(progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }

            showProgressBar = false;
        }
    }

    public void setProgress(int progress){
        this.progress = progress;

        if(progressBar != null) {
            progressBar.setProgress(progress);
        }
    }

    public void setText(String text){
        this.text = text;

        if(textView != null){
            textView.setText(text);
        }
    }

    protected void hideAllDivisors(){
        if(this.view != null) {
            View layoutUp = this.view.findViewById(R.id.linearLayoutUp);
            if (layoutUp != null) {
                layoutUp.setVisibility(View.GONE);
            }
            View divisorView = this.view.findViewById(R.id.divisorView);
            if (divisorView != null) {
                divisorView.setVisibility(View.GONE);
            }
            View layoutDown = this.view.findViewById(R.id.linearLayoutDown);
            if (layoutDown != null) {
                layoutDown.setVisibility(View.GONE);
            }
        }
    }

    protected void hideProgressBarLayout(){
        View divisorView = this.view.findViewById(R.id.divisorView);
        if (divisorView != null) {
            divisorView.setVisibility(View.GONE);
        }
        View layoutDown = this.view.findViewById(R.id.linearLayoutDown);
        if (layoutDown != null) {
            layoutDown.setVisibility(View.GONE);
        }
    }

    protected void showAllDivisors(){
        if(this.view != null) {
            View layoutUp = this.view.findViewById(R.id.linearLayoutUp);
            if (layoutUp != null) {
                layoutUp.setVisibility(View.VISIBLE);
            }
            View divisorView = this.view.findViewById(R.id.divisorView);
            if (divisorView != null) {
                divisorView.setVisibility(View.VISIBLE);
            }
            View layoutDown = this.view.findViewById(R.id.linearLayoutDown);
            if (layoutDown != null) {
                layoutDown.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setShowTitle(boolean show){
        showTitle = show;
    }

    public void setShowText(boolean show){
        showTextView = show;
    }

    public void setLayout(int layout){
        layoutRes = layout;
    }

    public boolean isDismissed(){
        return dismissed;
    }

    public Dialog getCustomDialog(){
        return dialog;
    }

    public boolean isRunning(){
        return isRunning;
    }

    public void setShowIndeterminate(boolean indeterminate){
        showIndeterminate = indeterminate;
    }

    public ProgressBar getProgressBar(){
        return progressBar;
    }

    public void setTextSize(float size){
        textSize = size;
    }
}
