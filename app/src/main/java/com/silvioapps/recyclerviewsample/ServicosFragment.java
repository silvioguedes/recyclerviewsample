package com.silvioapps.recyclerviewsample;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class ServicosFragment extends Fragment{
    private View view = null;
    private Activity activity = null;
    private double progress = 0;
    private boolean out = false;
    private Thread thread = null;
    private int visibility = View.VISIBLE;
    private static ServicosFragment instance = null;
    private Handler handler = null;
    private MyAsyncTask myAsyncTask = null;
    private CustomActionBarControls customActionBarControls = null;

    public static ServicosFragment getInstance(){
        if(instance == null){
            instance = new ServicosFragment();
        }

        return instance;
    }

    public static void destroyInstance(){
        if(instance != null){
            instance = null;
        }
    }

    public void setActivity(Activity activity){
        this.activity = activity;
    }

    public void preload(){
        handler = new Handler();
        progress = 0;

        //TODO

        progress = Constants.FINISH_PROGRESS;
    }

    public double getProgress(){
        return progress;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setRetainInstance(true);

        Utils.setWindowBackgroundColor(activity, R.color.colorBackground);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        this.view = layoutInflater.inflate(R.layout.fragment_main, viewGroup, false);
        this.view.setVisibility(visibility);

        return this.view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        if(getActivity() != null) {
            Utils.showActionBar((AppCompatActivity)getActivity());

            customActionBarControls = new CustomActionBarControls();
            customActionBarControls.setControls((AppCompatActivity) getActivity(), this);
            customActionBarControls.getTitleTextView().setText(R.string.services);
            customActionBarControls.getIconImageView().setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            if (thread != null) {
                thread.interrupt();
            }

            out = true;

            if(myAsyncTask != null){
                myAsyncTask.cancel(true);
            }
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    public void setVisibility(int visibility){
        this.visibility = visibility;

        if(this.view != null){
            this.view.setVisibility(visibility);
        }
    }

    private class MyAsyncTask extends AsyncTask<Object, Object, Object> {
        protected void onPreExecute() {}

        protected Object doInBackground(Object... ids) {
            return null;
        }

        protected void onPostExecute(Object result) {}
    }
}
