package com.silvioapps.recyclerviewsample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Silvio Guedes on 09/09/2016.
 */
public class ListaAdapter extends RecyclerView.Adapter<ListaAdapter.ViewHolder>{
    private List<String> valuesList = null;
    private Context context = null;
    private ViewHolder viewHolder = null;
    private static IRecyclerViewOnClickListener recyclerViewClickListener = null;

    public ListaAdapter(Context context, List<String> valuesList, IRecyclerViewOnClickListener listener){
        this.context = context;
        this.valuesList = valuesList;
        this.recyclerViewClickListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView textView = null;

        public ViewHolder(View view){
            super(view);

            view.setOnClickListener(this);

            textView = (TextView)view.findViewById(R.id.textView);
        }

        @Override
        public void onClick(View view) {
            if(recyclerViewClickListener != null) {
                recyclerViewClickListener.recyclerViewListClicked(view, this.getLayoutPosition());
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_layout,parent,false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        if(holder != null && holder.textView != null) {
            holder.textView.setText(valuesList.get(position));
        }
    }

    @Override
    public int getItemCount(){
        return valuesList == null ? 0 : valuesList.size();
    }
}
