package com.silvioapps.recyclerviewsample;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.ScrollView;

/**
 * Created by Silvio Guedes on 23/08/2016.
 */
public class Utils {
    /*public static void hideActionBar(AppCompatActivity activity){
        if(activity != null) {
            activity.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
            activity.getSupportActionBar().hide();
        }
    }

    public static void showActionBar(AppCompatActivity activity){
        if(activity != null) {
            activity.getSupportActionBar().show();
        }
    }*/

    public static View showCustomActionBar(AppCompatActivity activity, int resLayout){
        if(activity != null) {
            ActionBar actionBar = activity.getSupportActionBar();
            View view = activity.getLayoutInflater().inflate(resLayout, null);
            actionBar.setCustomView(view);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

            Toolbar toolbar = (Toolbar) actionBar.getCustomView().getParent();
            toolbar.setContentInsetsAbsolute(0, 0);

            return view;
        }

        return null;
    }

    public static void returnFragment(Activity activity, Fragment fragment){
        if (fragment != null && fragment.getFragmentManager() != null && fragment.getFragmentManager().getBackStackEntryCount() > 0) {
            fragment.getFragmentManager().popBackStack();
        } else {
            if (activity != null) {
                activity.finish();
            }
        }
    }

    public static void setWindowBackgroundColor(Activity activity, int resColor){
        if(activity != null) {
            int backgroundColor = ContextCompat.getColor(activity, resColor);
            ColorDrawable colorDrawable = new ColorDrawable(backgroundColor);
            activity.getWindow().setBackgroundDrawable(colorDrawable);
        }
    }

    public static Point getScreenSize(Context context){
        if(context != null) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            return new Point(width, height);
        }

        return null;
    }

    public static void setRatingBarColors(Context context, RatingBar ratingBar, int colorRatingBarStarFullySelected, int colorRatingBarStarPartiallySelected, int colorRatingBarStarNotSelected){
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ResourcesCompat.getColor(context.getResources(),colorRatingBarStarFullySelected,null), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(ResourcesCompat.getColor(context.getResources(),colorRatingBarStarPartiallySelected,null), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(ResourcesCompat.getColor(context.getResources(),colorRatingBarStarNotSelected,null), PorterDuff.Mode.SRC_ATOP);
    }

    public static void scrollToAView(final ScrollView scrollView, final View viewTarget){
        if(scrollView != null && viewTarget != null) {
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.scrollTo(0, (int) viewTarget.getY());
                }
            });
        }
    }

    public static void hideActionBar(AppCompatActivity activity){
        boolean r = activity.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        activity.getSupportActionBar().hide();
    }

    public static void showActionBar(AppCompatActivity activity){
        activity.getSupportActionBar().show();
    }

    public static int getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return Configuration.ORIENTATION_PORTRAIT;
            case Surface.ROTATION_90:
                return Configuration.ORIENTATION_LANDSCAPE;
            case Surface.ROTATION_180:
                return -Configuration.ORIENTATION_PORTRAIT;
            case Surface.ROTATION_270:
                return -Configuration.ORIENTATION_LANDSCAPE;
            default:
                return 0;
        }
    }
}
