package com.silvioapps.recyclerviewsample;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{
    private MainFragment fragment = null;
    private FragmentTransaction fragmentTransaction = null;
    private boolean out = false;
    private Thread thread = null;
    private Handler handler = null;
    private static boolean isRunning = false;

    private Thread orientationThread = null;
    private static boolean splashScreen = true;
    private boolean checkOrientation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.hideActionBar(this);
        setContentView(R.layout.activity_main);

        //EasyPermissions.requestPermissions(this,getString(R.string.request_permission),Constants.REQUEST_PERMISSION,Manifest.permission.);

        /*orientationThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (splashScreen){
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }

                while(!out && checkOrientation){
                    if(splashScreen){
                        //TODO NOTHING
                    }
                    else{
                        if(checkOrientation){
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                            checkOrientation = false;
                        }
                    }
                }
            }
        });
        if(checkOrientation) {
            orientationThread.start();
        }*/

        /*handler = new Handler();
        orientationThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!out && splashScreen){
                    //Log.i("TAG***","orientation "+getResources().getConfiguration().orientation);
                    //if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                    if(Utils.getRotation(MainActivity.this) == Configuration.ORIENTATION_LANDSCAPE || Utils.getRotation(MainActivity.this) == -Configuration.ORIENTATION_LANDSCAPE ){
                    //if(checkOrientation && getRequestedOrientation())
                        if (checkOrientation && handler != null) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.i("TAG***","ORIENTATION_LANDSCAPE");

                                    getWindow().setBackgroundDrawableResource(R.drawable.splash_screen_land);
                                }
                            });

                            checkOrientation = false;
                        }
                    }
                    else{
                        checkOrientation = true;
                    }
                }
            }
        });
        orientationThread.start();*/


        handler = new Handler();
        if(savedInstanceState == null) {
            createFragment(MainActivity.this);

            loadFragment();

            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(!out && fragment != null && fragment.getProgress() < Constants.FINISH_PROGRESS) {
                        //TODO NOTHING
                    }

                    if (handler != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                showFragment();
                            }
                        });
                    }
                }
            });
            thread.start();
        }
    }

    @Override
    public void onStart(){
        super.onStart();

        isRunning = true;
    }

    @Override
    public void onResume(){
        super.onResume();

        isRunning = true;
    }

    @Override
    public void onDestroy(){
        isRunning = false;

        super.onDestroy();

        if(!isChangingConfigurations()){
            out = true;

            if(thread != null){
                thread.interrupt();
            }

            CustomDialogFragment.destroyInstance();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        MyPermissions.permission = true;
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        MyPermissions.permission = false;
    }

    protected void createFragment(Activity activity){
        fragment = new MainFragment();
        fragment.setActivity(activity);
        fragment.setVisibility(View.GONE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }

    protected void loadFragment(){
        if(fragment != null) {
            fragment.preload();
        }
    }

    protected void showFragment(){
        splashScreen = false;

        Utils.showActionBar(this);

        if(fragment != null){
            fragment.setVisibility(View.VISIBLE);
        }
    }
}
