package com.silvioapps.recyclerviewsample;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Silvio Guedes on 13/09/2016.
 */
public class CustomActionBarControls {
    private View customActionBar = null;
    private TextView titleTextView = null;
    private ImageView iconImageView = null;
    private ImageButton returnImageButton = null;
    private ImageButton searchImageButton = null;

    public void setControls(final AppCompatActivity activity, final Fragment fragment){
        customActionBar = Utils.showCustomActionBar(activity,R.layout.action_bar_layout);

        titleTextView = (TextView)customActionBar.findViewById(R.id.title);
        titleTextView.setVisibility(View.VISIBLE);
        titleTextView.setText(R.string.app_name);

        iconImageView = (ImageView)customActionBar.findViewById(R.id.icon);
        iconImageView.setVisibility(View.VISIBLE);

        returnImageButton = (ImageButton)customActionBar.findViewById(R.id.action_return);
        returnImageButton.setVisibility(View.VISIBLE);
        returnImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.returnFragment(activity, fragment);
            }
        });

        searchImageButton = (ImageButton)customActionBar.findViewById(R.id.action_search);
        searchImageButton.setVisibility(View.VISIBLE);
    }

    public View getCustomActionBar() {
        return customActionBar;
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public ImageView getIconImageView() {
        return iconImageView;
    }

    public ImageButton getReturnImageButton() {
        return returnImageButton;
    }

    public ImageButton getSearchImageButton() {
        return searchImageButton;
    }
}
