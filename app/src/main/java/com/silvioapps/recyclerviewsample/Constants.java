package com.silvioapps.recyclerviewsample;

/**
 * Created by Silvio Guedes on 23/08/2016.
 */
public class Constants {
    public static final int FINISH_PROGRESS = 100;
    public static final long SPLASH_SCREEN_SLEEP = 5000;
    public static final int REQUEST_PERMISSION = 1;
    public static final String BASE_URL = "http://dev.4all.com:3003/";
    public static final int CAMERA_ZOOM = 12;
    public static final int PICTURE_HEIGHT_PERCENT = 35;
    public static final int MAP_HEIGHT_PERCENT = 15;
    public static final int ADDRESS_TEXT_VIEW_PADDING_RIGHT = 20;
    public static final int PICTURE_COMMENTER_BORDER = 3;
    public static final String SCHEME_ACTION_DIAL = "tel";
    public static final float DIALOG_TEXT_SIZE = 20.0f;
}
