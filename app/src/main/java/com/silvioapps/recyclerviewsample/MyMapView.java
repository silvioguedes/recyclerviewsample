package com.silvioapps.recyclerviewsample;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Silvio Guedes on 15/09/2016.
 */
public class MyMapView {
    private MapView mapView = null;
    private GoogleMap googleMap = null;
    private double latitude = 0.0;
    private double longitude = 0.0;
    private String markerTitle = "";
    private String markerDescription = "";
    private float cameraZoom = 0.0f;
    private boolean myLocationEnabled = false;
    private boolean isMapReady = false;

    public void startMapView(Bundle bundle, Context context, MapView mapView) {
        if(mapView != null && context != null) {
            isMapReady = false;

            mapView.onCreate(bundle);
            mapView.onResume(); // needed to get the map to display immediately

            try {
                MapsInitializer.initialize(context);
            } catch (Exception e) {
                e.printStackTrace();
            }

            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap mMap) {
                    if(mMap != null) {
                        isMapReady = true;
                        googleMap = mMap;

                        try {
                            // For showing a move to my location button
                            googleMap.setMyLocationEnabled(myLocationEnabled);
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }

                        // For dropping a marker at a point on the Map
                        LatLng latLng = new LatLng(latitude, longitude);
                        googleMap.addMarker(new MarkerOptions().position(latLng).title(markerTitle).snippet(markerDescription));

                        // For zooming automatically to the location of the marker
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(cameraZoom).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }
            });
        }
    }

    public void setSettings(double latitude, double longitude, String markerTitle, String markerDescription, float cameraZoom, boolean myLocationEnabled){
        this.latitude = latitude;
        this.longitude = longitude;
        this.markerTitle = markerTitle;
        this.markerDescription = markerDescription;
        this.cameraZoom = cameraZoom;
        this.myLocationEnabled = myLocationEnabled;
    }

    public GoogleMap getGoogleMap(){
        return googleMap;
    }

    public boolean isMapReady(){
        return isMapReady;
    }
}
