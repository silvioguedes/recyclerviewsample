package com.silvioapps.recyclerviewsample;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

/**
 * Created by Silvio Guedes on 09/09/2016.
 */
public class ComentariosAdapter extends RecyclerView.Adapter<ComentariosAdapter.ViewHolder>{
    private List<Item.Comentarios> valuesList = null;
    private Context context = null;
    private ViewHolder viewHolder = null;
    private static IRecyclerViewOnClickListener recyclerViewClickListener = null;

    public ComentariosAdapter(Context context, List<Item.Comentarios> valuesList, IRecyclerViewOnClickListener listener){
        this.context = context;
        this.valuesList = valuesList;
        this.recyclerViewClickListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView pictureCommenterImageView = null;
        private TextView nameCommenterTextView = null;
        private TextView titleCommenterTextView = null;
        private TextView commentCommenterTextView = null;
        private RatingBar ratingCommenterRatingBar = null;

        public ViewHolder(View view){
            super(view);

            view.setOnClickListener(this);

            pictureCommenterImageView = (ImageView) view.findViewById(R.id.pictureCommenterImageView);
            nameCommenterTextView = (TextView) view.findViewById(R.id.nameCommenterTextView);
            titleCommenterTextView = (TextView) view.findViewById(R.id.titleCommenterTextView);
            ratingCommenterRatingBar = (RatingBar) view.findViewById(R.id.ratingCommenterRatingBar);
            commentCommenterTextView = (TextView) view.findViewById(R.id.commentCommenterTextView);
        }

        @Override
        public void onClick(View view) {
            if(recyclerViewClickListener != null) {
                recyclerViewClickListener.recyclerViewListClicked(view, this.getLayoutPosition());
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comentarios_layout,parent,false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position){
        if(viewHolder != null && valuesList != null) {
            setBitmapImageView(viewHolder, position);
            viewHolder.nameCommenterTextView.setText(valuesList.get(position).getNome());
            viewHolder.titleCommenterTextView.setText(valuesList.get(position).getTitulo());
            viewHolder.commentCommenterTextView.setText(valuesList.get(position).getComentario());
            viewHolder.ratingCommenterRatingBar.setRating(valuesList.get(position).getNota());
            Utils.setRatingBarColors(context,viewHolder.ratingCommenterRatingBar,
                    R.color.colorRatingBarStarFullySelected,
                    R.color.colorRatingBarStarPartiallySelected,
                    R.color.colorRatingBarStarNotSelected);
        }
    }

    @Override
    public int getItemCount(){
        return valuesList == null ? 0 : valuesList.size();
    }

    protected void setBitmapImageView(ViewHolder viewHolder, int position){
        if (context != null && viewHolder.pictureCommenterImageView != null) {
            Transformation transformation = new RoundedTransformationBuilder()
                    .borderColor(Color.TRANSPARENT)
                    .borderWidthDp(Constants.PICTURE_COMMENTER_BORDER)
                    .oval(true)
                    .build();

            Picasso.with(context)
                    .load(valuesList.get(position).getUrlFoto())
                    .transform(transformation)
                    .placeholder(R.drawable.no_image_yet)
                    .error(R.drawable.error_no_image)
                    .into(viewHolder.pictureCommenterImageView);
        }
    }
}
