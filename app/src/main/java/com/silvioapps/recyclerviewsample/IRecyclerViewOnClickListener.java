package com.silvioapps.recyclerviewsample;

import android.view.View;

/**
 * Created by Silvio Guedes on 23/08/2016.
 */
public interface IRecyclerViewOnClickListener
{
    public void recyclerViewListClicked(View v, int position);
}

