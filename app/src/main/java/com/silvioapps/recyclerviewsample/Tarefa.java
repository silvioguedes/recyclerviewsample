package com.silvioapps.recyclerviewsample;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Silvio Guedes on 13/09/2016.
 */
public class Tarefa{
    public class Lista{
        @SerializedName("lista")
        private List<String> list;

        public List<String> getList() {
            return list;
        }

        public void setList(List<String> list) {
            this.list = list;
        }
    }

    public interface TarefaAPI {
        @GET("tarefa/")
        Call<Lista> getObject();
    }
}

