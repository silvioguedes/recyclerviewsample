package com.silvioapps.recyclerviewsample;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Silvio Guedes on 10/07/2016.
 */
public class MainFragment extends Fragment implements IRecyclerViewOnClickListener {
    private View view = null;
    private Activity activity = null;
    private double progress = 0;
    private boolean out = false;
    private Thread thread = null;
    private int visibility = View.VISIBLE;
    private static MainFragment instance = null;
    private Handler handler = null;
    private MyAsyncTask myAsyncTask = null;
    private RecyclerView recyclerView = null;
    private ListaAdapter listaAdapter = null;
    private List<String> tarefasIDList = null;
    private LinearLayoutManager linearLayoutManager = null;
    private ItemFragment itemFragment = null;
    private CustomActionBarControls customActionBarControls = null;
    private CustomDialogFragment customDialogFragment = null;

    public static MainFragment getInstance(){
        if(instance == null){
            instance = new MainFragment();
        }

        return instance;
    }

    public static void destroyInstance(){
        if(instance != null){
            instance = null;
        }
    }

    public void setActivity(Activity activity){
        this.activity = activity;
    }

    public void preload(){
        long startTime = System.currentTimeMillis();
        handler = new Handler();
        progress = 0;

        long endTime = System.currentTimeMillis();
        long total = endTime - startTime;
        if(total < Constants.SPLASH_SCREEN_SLEEP) {
            try {
                Thread.sleep(Constants.SPLASH_SCREEN_SLEEP);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        progress = Constants.FINISH_PROGRESS;
    }

    public double getProgress(){
        return progress;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setRetainInstance(true);

        Utils.setWindowBackgroundColor(activity, R.color.colorBackgroundList);

        myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute();

        tarefasIDList = new ArrayList<>();
        tarefasIDList.clear();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        if(bundle == null){
            createCustomDialogFragment();
        }

        this.view = layoutInflater.inflate(R.layout.fragment_main, viewGroup, false);

        if(this.view != null){
            this.view.setVisibility(visibility);

            if(activity != null) {
                recyclerView = (RecyclerView) this.view.findViewById(R.id.recyclerView);
                linearLayoutManager = new LinearLayoutManager(activity);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setHasFixedSize(true);
                listaAdapter = new ListaAdapter(activity, tarefasIDList, this);
                recyclerView.setAdapter(listaAdapter);
            }
        }

        return this.view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        if(getActivity() != null) {
            Utils.showActionBar((AppCompatActivity)getActivity());

            customActionBarControls = new CustomActionBarControls();
            customActionBarControls.setControls((AppCompatActivity) getActivity(), this);
            customActionBarControls.getTitleTextView().setText(R.string.list);
            customActionBarControls.getIconImageView().setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            if (thread != null) {
                thread.interrupt();
            }

            out = true;

            if(myAsyncTask != null){
                myAsyncTask.cancel(true);
            }

            destroyCustomDialogFragment();
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        createItemFragment(getActivity());

        showCustomDialogFragment();

        loadItemFragment(position);

        waitFragmentBeReadyThread();
    }

    protected void waitFragmentBeReadyThread(){
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!out && itemFragment != null && itemFragment.getProgress() < Constants.FINISH_PROGRESS){
                    //TODO WAIT
                }

                if(handler != null){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            dismissCustomDialogFragment();

                            showItemFragment();
                        }
                    });
                }
            }
        });
        thread.start();
    }

    protected void createCustomDialogFragment(){
        customDialogFragment = new CustomDialogFragment();
        customDialogFragment.setShowProgressBar(false);
        customDialogFragment.setShowIndeterminate(true);
        customDialogFragment.setShowTitle(false);
        customDialogFragment.setShowText(false);
        customDialogFragment.setLayout(R.layout.custom_dialog_fragment_layout);
        customDialogFragment.create(getFragmentManager(),CustomDialogFragment.FRAGMENT_TAG);
    }

    protected void showCustomDialogFragment(){
        if(customDialogFragment != null){
            customDialogFragment.show();
        }
    }

    protected void dismissCustomDialogFragment(){
        if(customDialogFragment != null){
            customDialogFragment.dismissDialog();
        }
    }

    protected void destroyCustomDialogFragment(){
        if(customDialogFragment != null){
            customDialogFragment.destroyDialog();
        }
    }

    protected void createItemFragment(Activity activity){
        itemFragment = new ItemFragment();
        itemFragment.setActivity(activity);
        itemFragment.setVisibility(View.GONE);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, itemFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    protected void loadItemFragment(int position){
        if(itemFragment != null && tarefasIDList != null && tarefasIDList.size() > 0) {
            itemFragment.preload(tarefasIDList.get(position));
        }
    }

    protected void showItemFragment(){
        if(itemFragment != null){
            itemFragment.setVisibility(View.VISIBLE);
        }
    }

    public void setVisibility(int visibility){
        this.visibility = visibility;

        if(this.view != null){
            this.view.setVisibility(visibility);
        }
    }

    private class MyAsyncTask extends AsyncTask<Object, Object, Tarefa.Lista> {
        private Tarefa.Lista tarefa = null;
        private boolean completed = false;
        private Retrofit retrofit = null;

        protected void onPreExecute() {
            completed = false;

            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        protected Tarefa.Lista doInBackground(Object... urls) {
            if(retrofit != null) {
                searchAsynchronously(retrofit);
            }

            return tarefa;
        }

        protected void onPostExecute(Tarefa.Lista result) {
            if(tarefasIDList != null && tarefasIDList.isEmpty() && result != null) {
                tarefasIDList.addAll(result.getList());
            }

            if (listaAdapter != null) {
                listaAdapter.notifyDataSetChanged();
            }
        }

        protected void searchAsynchronously(Retrofit retrofit) {
            try {
                Tarefa.TarefaAPI service = retrofit.create(Tarefa.TarefaAPI.class);
                Call<Tarefa.Lista> call = service.getObject();
                call.enqueue(new Callback<Tarefa.Lista>() {
                    @Override
                    public void onResponse(Call<Tarefa.Lista> call, Response<Tarefa.Lista> response) {
                        tarefa = response.body();

                        completed = true;
                    }

                    @Override
                    public void onFailure(Call<Tarefa.Lista> call, Throwable t) {
                        Log.e("TAG", "onFailure " + t.getMessage());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            while(!out && !completed){
                //TODO NOTHING
            }
        }
    }
}
